/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.mapper.system


import wang.encoding.mroot.model.entity.system.ScheduleJob
import wang.encoding.mroot.common.mapper.SuperMapper


/**
 * 定时任务 Mapper 接口层
 *
 * @author ErYang
 */
interface ScheduleJobMapper : SuperMapper<ScheduleJob> {


}

// -----------------------------------------------------------------------------------------------------

// End ScheduleJobMapper interface

/* End of file ScheduleJobMapper.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/mapper/system/ScheduleJobMapper.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
