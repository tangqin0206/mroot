/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.constant


/**
 * 请求日志 常量类
 *
 * @author ErYang
 */
object RequestLogConstant {


    /**
     * 后台管理模块
     */
    const val ADMIN_MODULE: String = "message.aop.requestLog.module.admin"
    /**
     * 登录页面
     */
    const val ADMIN_MODULE_CHANGE_LANGUAGE_VIEW: String = "message.aop.requestLog.module.admin.changeLanguage.view"
    const val ADMIN_MODULE_LOGIN_VIEW: String = "message.aop.requestLog.module.admin.login.view"
    const val ADMIN_MODULE_LOGIN_DO_LOGIN: String = "message.aop.requestLog.module.admin.login.doLogin"
    const val ADMIN_MODULE_LOGOUT: String = "message.aop.requestLog.module.admin.logout.view"
    /**
     * 主页
     */
    const val ADMIN_MODULE_INDEX_VIEW: String = "message.aop.requestLog.module.admin.index.view"
    /**
     * 代码生成
     */
    const val ADMIN_MODULE_GENERATE_VIEW: String = "message.aop.requestLog.module.admin.generate.view"
    const val ADMIN_MODULE_GENERATE_CODE_VIEW: String = "message.aop.requestLog.module.admin.generate.code.view"
    const val ADMIN_MODULE_GENERATE_SAVE: String = "message.aop.requestLog.module.admin.generate.save"
    /**
     * 用户
     */
    const val ADMIN_MODULE_USER_INDEX: String = "message.aop.requestLog.module.admin.user.index"
    const val ADMIN_MODULE_USER_ADD: String = "message.aop.requestLog.module.admin.user.add"
    const val ADMIN_MODULE_USER_SAVE: String = "message.aop.requestLog.module.admin.user.save"
    const val ADMIN_MODULE_USER_EDIT: String = "message.aop.requestLog.module.admin.user.edit"
    const val ADMIN_MODULE_USER_UPDATE: String = "message.aop.requestLog.module.admin.user.update"
    const val ADMIN_MODULE_USER_VIEW: String = "message.aop.requestLog.module.admin.user.view"
    const val ADMIN_MODULE_USER_DELETE: String = "message.aop.requestLog.module.admin.user.delete"
    const val ADMIN_MODULE_USER_DELETE_BATCH: String = "message.aop.requestLog.module.admin.user.delete.batch"
    const val ADMIN_MODULE_USER_AUTHORIZATION: String = "message.aop.requestLog.module.admin.user.authorization"
    const val ADMIN_MODULE_USER_AUTHORIZATION_SAVE: String = "message.aop.requestLog.module.admin.user.authorization.save"
    const val ADMIN_MODULE_USER_RECYCLE_BIN_INDEX: String = "message.aop.requestLog.module.admin.user.recycleBin"
    const val ADMIN_MODULE_USER_RECOVER: String = "message.aop.requestLog.module.admin.user.recover"
    const val ADMIN_MODULE_USER_RECOVER_BATCH: String = "message.aop.requestLog.module.admin.user.recover.batch"
    const val ADMIN_MODULE_USER_PASSWORD: String = "message.aop.requestLog.module.admin.user.password"
    const val ADMIN_MODULE_USER_PASSWORD_UPDATE: String = "message.aop.requestLog.module.admin.user.password.update"
    const val ADMIN_MODULE_USER_NICK_NAME: String = "message.aop.requestLog.module.admin.user.nickName"
    const val ADMIN_MODULE_USER_NICK_NAME_UPDATE: String = "message.aop.requestLog.module.admin.user.nickName.update"
    /**
     * 角色
     */
    const val ADMIN_MODULE_ROLE_INDEX: String = "message.aop.requestLog.module.admin.role.index"
    const val ADMIN_MODULE_ROLE_ADD: String = "message.aop.requestLog.module.admin.role.add"
    const val ADMIN_MODULE_ROLE_SAVE: String = "message.aop.requestLog.module.admin.role.save"
    const val ADMIN_MODULE_ROLE_EDIT: String = "message.aop.requestLog.module.admin.role.edit"
    const val ADMIN_MODULE_ROLE_UPDATE: String = "message.aop.requestLog.module.admin.role.update"
    const val ADMIN_MODULE_ROLE_VIEW: String = "message.aop.requestLog.module.admin.role.view"
    const val ADMIN_MODULE_ROLE_DELETE: String = "message.aop.requestLog.module.admin.role.delete"
    const val ADMIN_MODULE_ROLE_DELETE_BATCH: String = "message.aop.requestLog.module.admin.role.delete.batch"
    const val ADMIN_MODULE_ROLE_AUTHORIZATION: String = "message.aop.requestLog.module.admin.role.authorization"
    const val ADMIN_MODULE_ROLE_AUTHORIZATION_SAVE: String = "message.aop.requestLog.module.admin.role.authorization.save"
    const val ADMIN_MODULE_ROLE_RECYCLE_BIN_INDEX: String = "message.aop.requestLog.module.admin.role.recycleBin"
    const val ADMIN_MODULE_ROLE_RECOVER: String = "message.aop.requestLog.module.admin.role.recover"
    const val ADMIN_MODULE_ROLE_RECOVER_BATCH: String = "message.aop.requestLog.module.admin.role.recover.batch"

    /**
     * 权限
     */
    const val ADMIN_MODULE_RULE_INDEX: String = "message.aop.requestLog.module.admin.rule.index"
    const val ADMIN_MODULE_RULE_ADD: String = "message.aop.requestLog.module.admin.rule.add"
    const val ADMIN_MODULE_RULE_ADD_CHILDREN: String = "message.aop.requestLog.module.admin.rule.add.children"
    const val ADMIN_MODULE_RULE_SAVE: String = "message.aop.requestLog.module.admin.rule.save"
    const val ADMIN_MODULE_RULE_EDIT: String = "message.aop.requestLog.module.admin.rule.edit"
    const val ADMIN_MODULE_RULE_UPDATE: String = "message.aop.requestLog.module.admin.rule.update"
    const val ADMIN_MODULE_RULE_VIEW: String = "message.aop.requestLog.module.admin.rule.view"
    const val ADMIN_MODULE_RULE_DELETE: String = "message.aop.requestLog.module.admin.rule.delete"
    const val ADMIN_MODULE_RULE_DELETE_BATCH: String = "message.aop.requestLog.module.admin.rule.delete.batch"
    const val ADMIN_MODULE_RULE_RECYCLE_BIN_INDEX: String = "message.aop.requestLog.module.admin.rule.recycleBin"
    const val ADMIN_MODULE_RULE_RECOVER: String = "message.aop.requestLog.module.admin.rule.recover"
    const val ADMIN_MODULE_RULE_RECOVER_BATCH: String = "message.aop.requestLog.module.admin.rule.recover.batch"

    /**
     * 请求日志
     */
    const val ADMIN_MODULE_REQUEST_LOG_INDEX: String = "message.aop.requestLog.module.admin.requestLog.index"
    const val ADMIN_MODULE_REQUEST_LOG_VIEW: String = "message.aop.requestLog.module.admin.requestLog.view"
    const val ADMIN_MODULE_REQUEST_LOG_DELETE: String = "message.aop.requestLog.module.admin.requestLog.delete"
    const val ADMIN_MODULE_REQUEST_LOG_DELETE_BATCH: String = "message.aop.requestLog.module.admin.requestLog.delete.batch"
    const val ADMIN_MODULE_REQUEST_LOG_TRUNCATE: String = "message.aop.requestLog.module.admin.requestLog.truncate"

    /**
     * logback日志
     */
    const val ADMIN_MODULE_LOGGING_EVENT_INDEX: String = "message.aop.requestLog.module.admin.loggingEvent.index"
    const val ADMIN_MODULE_LOGGING_EVENT_VIEW: String = "message.aop.requestLog.module.admin.loggingEvent.view"
    const val ADMIN_MODULE_LOGGING_EVENT_DELETE: String = "message.aop.requestLog.module.admin.loggingEvent.delete"
    const val ADMIN_MODULE_LOGGING_EVENT_DELETE_BATCH: String = "message.aop.requestLog.module.admin.loggingEvent.delete.batch"


    /**
     * 系统配置
     */
    const val ADMIN_MODULE_CONFIG_INDEX: String = "message.aop.requestLog.module.admin.config.index"
    const val ADMIN_MODULE_CONFIG_ADD: String = "message.aop.requestLog.module.admin.config.add"
    const val ADMIN_MODULE_CONFIG_SAVE: String = "message.aop.requestLog.module.admin.config.save"
    const val ADMIN_MODULE_CONFIG_EDIT: String = "message.aop.requestLog.module.admin.config.edit"
    const val ADMIN_MODULE_CONFIG_UPDATE: String = "message.aop.requestLog.module.admin.config.update"
    const val ADMIN_MODULE_CONFIG_VIEW: String = "message.aop.requestLog.module.admin.config.view"
    const val ADMIN_MODULE_CONFIG_DELETE: String = "message.aop.requestLog.module.admin.config.delete"
    const val ADMIN_MODULE_CONFIG_DELETE_BATCH: String = "message.aop.requestLog.module.admin.config.delete.batch"
    const val ADMIN_MODULE_CONFIG_RECYCLE_BIN_INDEX: String = "message.aop.requestLog.module.admin.config.recycleBin"
    const val ADMIN_MODULE_CONFIG_RECOVER: String = "message.aop.requestLog.module.admin.config.recover"
    const val ADMIN_MODULE_CONFIG_RECOVER_BATCH: String = "message.aop.requestLog.module.admin.config.recover.batch"
    const val ADMIN_MODULE_CONFIG_CLEAR_CACHE: String = "message.aop.requestLog.module.admin.config.clearCache"

    /**
     * 文章分类
     */
    const val ADMIN_MODULE_CMS_CATEGORY_INDEX: String =
            "message.aop.requestLog.module.admin.cms.category.index"
    const val ADMIN_MODULE_CMS_CATEGORY_ADD: String =
            "message.aop.requestLog.module.admin.cms.category.add"
    const val ADMIN_MODULE_CMS_CATEGORY_SAVE: String =
            "message.aop.requestLog.module.admin.cms.category.save"
    const val ADMIN_MODULE_CMS_CATEGORY_EDIT: String =
            "message.aop.requestLog.module.admin.cms.category.edit"
    const val ADMIN_MODULE_CMS_CATEGORY_UPDATE: String =
            "message.aop.requestLog.module.admin.cms.category.update"
    const val ADMIN_MODULE_CMS_CATEGORY_VIEW: String =
            "message.aop.requestLog.module.admin.cms.category.view"
    const val ADMIN_MODULE_CMS_CATEGORY_DELETE: String =
            "message.aop.requestLog.module.admin.cms.category.delete"
    const val ADMIN_MODULE_CMS_CATEGORY_DELETE_BATCH: String =
            "message.aop.requestLog.module.admin.cms.category.delete.batch"
    const val ADMIN_MODULE_CMS_CATEGORY_RECYCLE_BIN_INDEX: String =
            "message.aop.requestLog.module.admin.cms.category.recycleBin"
    const val ADMIN_MODULE_CMS_CATEGORY_RECOVER: String =
            "message.aop.requestLog.module.admin.cms.category.recover"
    const val ADMIN_MODULE_CMS_CATEGORY_RECOVER_BATCH: String =
            "message.aop.requestLog.module.admin.cms.category.recover.batch"

    /**
     * 文章
     */
    const val ADMIN_MODULE_CMS_ARTICLE_INDEX: String =
            "message.aop.requestLog.module.admin.cms.article.index"
    const val ADMIN_MODULE_CMS_ARTICLE_ADD: String =
            "message.aop.requestLog.module.admin.cms.article.add"
    const val ADMIN_MODULE_CMS_ARTICLE_SAVE: String =
            "message.aop.requestLog.module.admin.cms.article.save"
    const val ADMIN_MODULE_CMS_ARTICLE_EDIT: String =
            "message.aop.requestLog.module.admin.cms.article.edit"
    const val ADMIN_MODULE_CMS_ARTICLE_UPDATE: String =
            "message.aop.requestLog.module.admin.cms.article.update"
    const val ADMIN_MODULE_CMS_ARTICLE_VIEW: String =
            "message.aop.requestLog.module.admin.cms.article.view"
    const val ADMIN_MODULE_CMS_ARTICLE_DELETE: String =
            "message.aop.requestLog.module.admin.cms.article.delete"
    const val ADMIN_MODULE_CMS_ARTICLE_DELETE_BATCH: String =
            "message.aop.requestLog.module.admin.cms.article.delete.batch"
    const val ADMIN_MODULE_CMS_ARTICLE_RECYCLE_BIN_INDEX: String =
            "message.aop.requestLog.module.admin.cms.article.recycleBin"
    const val ADMIN_MODULE_CMS_ARTICLE_RECOVER: String =
            "message.aop.requestLog.module.admin.cms.article.recover"
    const val ADMIN_MODULE_CMS_ARTICLE_RECOVER_BATCH: String =
            "message.aop.requestLog.module.admin.cms.article.recover.batch"
    /**
     * 博客
     */
    const val ADMIN_MODULE_CMS_BLOG_INDEX: String =
            "message.aop.requestLog.module.admin.cms.blog.index"
    const val ADMIN_MODULE_CMS_BLOG_VIEW: String =
            "message.aop.requestLog.module.admin.cms.blog.view"


    /**
     * 定时任务
     */
    const val ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_INDEX: String =
            "message.aop.requestLog.module.admin.system.scheduleJob.index"
    const val ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_ADD: String =
            "message.aop.requestLog.module.admin.system.scheduleJob.add"
    const val ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_SAVE: String =
            "message.aop.requestLog.module.admin.system.scheduleJob.save"
    const val ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_EDIT: String =
            "message.aop.requestLog.module.admin.system.scheduleJob.edit"
    const val ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_UPDATE: String =
            "message.aop.requestLog.module.admin.system.scheduleJob.update"
    const val ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_VIEW: String =
            "message.aop.requestLog.module.admin.system.scheduleJob.view"
    const val ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_DELETE: String =
            "message.aop.requestLog.module.admin.system.scheduleJob.delete"
    const val ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_RUN: String =
            "message.aop.requestLog.module.admin.system.scheduleJob.run"
    const val ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_PAUSE: String =
            "message.aop.requestLog.module.admin.system.scheduleJob.pause"
    const val ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_RESUME: String =
            "message.aop.requestLog.module.admin.system.scheduleJob.resume"
    /**
     * 定时任务记录
     */
    const val ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_LOG_INDEX: String =
            "message.aop.requestLog.module.admin.system.scheduleJobLog.index"
    const val ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_LOG_VIEW: String =
            "message.aop.requestLog.module.admin.system.scheduleJobLog.view"
    const val ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_LOG_DELETE: String =
            "message.aop.requestLog.module.admin.system.scheduleJobLog.delete"
    const val ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_LOG_DELETE_BATCH: String =
            "message.aop.requestLog.module.admin.system.scheduleJobLog.delete.batch"

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RequestLogConstant class

/* End of file RequestLogConstant.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/common/constant/RequestLogConstant.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
