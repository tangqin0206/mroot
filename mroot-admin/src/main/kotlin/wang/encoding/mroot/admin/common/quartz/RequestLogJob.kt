/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.quartz


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import wang.encoding.mroot.service.system.RequestLogService


/**
 * 后台 请求日志 定时任务
 *
 * @author ErYang
 */
@Component
class RequestLogJob {

    @Autowired
    private lateinit var requestLogService: RequestLogService

    // -------------------------------------------------------------------------------------------------

    /**
     * 定时任务 执行删除 一次删除 2 条
     */
    fun removeSize2() {
        val size = 2
        requestLogService.remove2QuartzJob(size)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 定时任务 执行删除 一次删除 5 条
     */
    fun removeSize5() {
        val size = 5
        requestLogService.remove2QuartzJob(size)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 定时任务 执行删除 一次删除 10 条
     */
    fun removeSize10() {
        val size = 10
        requestLogService.remove2QuartzJob(size)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 定时任务 执行删除 一次删除 20 条
     */
    fun removeSize20() {
        val size = 20
        requestLogService.remove2QuartzJob(size)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 定时任务删除
     *
     * @param size Int 数量
     */
    private fun remove(size: Int) {
        requestLogService.remove2QuartzJob(size)
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RequestLogJob class

/* End of file RequestLogJob.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/quartz/RequestLogJob.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
