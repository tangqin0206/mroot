/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.config


import cn.org.rapid_framework.freemarker.directive.BlockDirective
import cn.org.rapid_framework.freemarker.directive.ExtendsDirective
import cn.org.rapid_framework.freemarker.directive.OverrideDirective
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Configuration
import wang.encoding.mroot.admin.common.constant.ConfigConst
import wang.encoding.mroot.admin.common.freemarker.modelex.I18nMethodModelEx
import wang.encoding.mroot.common.config.LocaleMessageSourceConfiguration


import javax.annotation.PostConstruct
import com.jagregory.shiro.freemarker.ShiroTags
import wang.encoding.mroot.admin.common.freemarker.modelex.CategoryMethodModelEx
import wang.encoding.mroot.admin.common.freemarker.modelex.RuleMethodModelEx
import wang.encoding.mroot.service.cms.CategoryService
import wang.encoding.mroot.service.system.RuleService


/**
 * freemarker 配置
 *
 * @author ErYang
 */
@Configuration
@EnableCaching
class FreemarkerConfiguration
@Autowired
constructor(private val configuration: freemarker.template.Configuration,
            private val localeMessageSourceConfiguration: LocaleMessageSourceConfiguration,
            private val ruleService: RuleService,
            private val categoryService: CategoryService
) {

    @Autowired
    private lateinit var configConst: ConfigConst

    @PostConstruct
    fun setSharedVariable() {
        // 国际化信息
        configuration.setSharedVariable(configConst.i18nName, I18nMethodModelEx(localeMessageSourceConfiguration))
        // 模板继承
        configuration.setSharedVariable(configConst.freemarkerSharedVariableBlockName, BlockDirective())
        configuration.setSharedVariable(configConst.freemarkerSharedVariableOverrideName, OverrideDirective())
        configuration.setSharedVariable(configConst.freemarkerSharedVariableExtendsName, ExtendsDirective())

        // shiro 标签
        configuration.setSharedVariable(configConst.shiroName, ShiroTags())

        // 权限信息
        configuration.setSharedVariable(configConst.ruleName, RuleMethodModelEx(ruleService))

        // 文章分类信息
        configuration.setSharedVariable(configConst.categoryName, CategoryMethodModelEx(categoryService))
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End FreemarkerConfiguration class

/* End of file FreemarkerConfiguration.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/config/FreemarkerConfiguration.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
