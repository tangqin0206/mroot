/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.kaptcha


import com.google.code.kaptcha.Constants
import com.google.code.kaptcha.Producer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.ModelAndView
import wang.encoding.mroot.admin.common.controller.BaseAdminController
import wang.encoding.mroot.common.exception.ControllerException
import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import javax.servlet.ServletOutputStream
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpSession


/**
 * 验证控制器
 *
 * @author ErYang
 */
@RestController
class KaptchaController : BaseAdminController() {

    @Autowired
    private lateinit var kaptchaProducer: Producer

    /**
     * 获取验证码图片
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @Suppress("NAME_SHADOWING")
    @RequestMapping(value = ["/verifyImage"])
    @Throws(ControllerException::class)
    fun verifyImage(request: HttpServletRequest, response: HttpServletResponse): ModelAndView? {

        val session: HttpSession = request.session
        response.setDateHeader("Expires", 0)

        //  HTTP/1.1 no-cache headers.
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate")

        //  HTTP/1.1 no-cache headers (use addHeader).
        response.addHeader("Cache-Control", "post-check=0, pre-check=0")

        //  HTTP/1.0 no-cache header.
        response.setHeader("Pragma", "no-cache")

        // 图片文件
        response.contentType = "image/jpeg"

        // 验证码
        val codeText: String = kaptchaProducer.createText()
        // 验证码放入 session 中
        session.setAttribute(Constants.KAPTCHA_SESSION_KEY, codeText)

        // 创建验证码图片
        val bufferedImage: BufferedImage = kaptchaProducer.createImage(codeText)
        val servletOutputStream: ServletOutputStream = response.outputStream

        // 输出图片
        ImageIO.write(bufferedImage, "png", servletOutputStream)
        servletOutputStream.use { servletOutputStream ->
            servletOutputStream.flush()
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End KaptchaController class

/* End of file KaptchaController.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/controller/kaptcha/KaptchaController.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
