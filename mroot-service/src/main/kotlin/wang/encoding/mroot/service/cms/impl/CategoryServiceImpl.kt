/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.cms.impl


import org.slf4j.Logger
import org.slf4j.LoggerFactory
import wang.encoding.mroot.common.service.BaseServiceImpl
import wang.encoding.mroot.common.util.HibernateValidationUtil
import wang.encoding.mroot.model.entity.cms.Category
import wang.encoding.mroot.model.enums.StatusEnum
import org.springframework.stereotype.Service
import wang.encoding.mroot.mapper.cms.CategoryMapper
import wang.encoding.mroot.service.cms.CategoryService

import java.math.BigInteger
import java.time.Instant
import java.util.Date


/**
 * 后台 文章分类 Service 接口实现类
 *
 * @author ErYang
 */
@Service
class CategoryServiceImpl : BaseServiceImpl
<CategoryMapper, Category>(), CategoryService {

    companion object {
        /**
         * logger
         */
        private val logger: Logger = LoggerFactory.getLogger(CategoryServiceImpl::class.java)
    }

    /**
     * 初始化新增 Category 对象
     *
     * @param category Category
     * @return Category
     */
    override fun initSaveCategory(category: Category): Category {
        category.status = StatusEnum.NORMAL.key
        category.gmtCreate = Date.from(Instant.now())
        return category
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化修改 Category 对象
     *
     * @param category Category
     * @return Category
     */
    override fun initEditCategory(category: Category): Category {
        category.gmtModified = Date.from(Instant.now())
        return category
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     */
    override fun validationCategory(category: Category): String? {
        return HibernateValidationUtil.validateEntity(category)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 category
     *
     * @param name 标识
     * @return Category
     */
    override fun getByName(name: String): Category? {
        val category = Category()
        category.name = name
        return super.getByModel(category)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 category
     *
     * @param title 名称
     * @return Category
     */
    override fun getByTitle(title: String): Category? {
        val category = Category()
        category.title = title
        return super.getByModel(category)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 文章分类
     *
     * @param category Category
     * @return ID  BigInteger
     */
    override fun saveBackId(category: Category): BigInteger? {
        val id: Int? = super.save(category)
        if (null != id && 0 < id) {
            return category.id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 文章分类(更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    override fun removeBackId(id: BigInteger): BigInteger? {
        val category = Category()
        category.id = id
        category.status = StatusEnum.DELETE.key
        category.gmtModified = Date.from(Instant.now())
        val backId: Int? = super.remove2StatusById(category)
        if (null != backId && 0 < backId) {
            return id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复 文章分类 (更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    override fun recoverBackId(id: BigInteger): BigInteger? {
        val category = Category()
        category.id = id
        category.status = StatusEnum.NORMAL.key
        category.gmtModified = Date.from(Instant.now())
        val backId: Int? = super.recover2StatusById(category)
        if (null != backId && 0 < backId) {
            return id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 文章分类
     *
     * @param category Category
     * @return ID  BigInteger
     */
    override fun updateBackId(category: Category): BigInteger? {
        val flag: Boolean = super.updateById(category)
        if (flag) {
            return category.id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 类型小于 3 文章分类集合
     *
     * @return  List<Category>
     */
    override fun listTypeLt3(): List<Category>? {
        val list: List<Category>? = superMapper!!.listTypeLt3()
        if (null != list && list.isNotEmpty()) {
            return Category.list2Tree(list)!!
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End CategoryServiceImpl class

/* End of file CategoryServiceImpl.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/service/cms/impl/CategoryServiceImpl.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
