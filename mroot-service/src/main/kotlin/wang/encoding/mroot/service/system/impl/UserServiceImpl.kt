/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.system.impl


import org.springframework.stereotype.Service
import wang.encoding.mroot.common.business.ResultData
import wang.encoding.mroot.common.service.BaseServiceImpl
import wang.encoding.mroot.common.util.HibernateValidationUtil
import wang.encoding.mroot.mapper.system.UserMapper
import wang.encoding.mroot.model.entity.system.User
import wang.encoding.mroot.model.enums.StatusEnum
import wang.encoding.mroot.service.system.UserService
import java.math.BigInteger
import java.time.Instant
import java.util.*


/**
 * 用户 service 实现类
 *
 * @author ErYang
 */
@Service
class UserServiceImpl : BaseServiceImpl<UserMapper, User>(), UserService {


    /**
     * 初始化新增 User 对象
     *
     * @param user User
     * @return User
     */
    override fun initSaveUser(user: User): User {
        user.status = StatusEnum.NORMAL.key
        user.gmtCreate = Date.from(Instant.now())
        return user
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化修改 User 对象
     *
     * @param user User
     * @return User
     */
    override fun initEditUser(user: User): User {
        user.gmtModified = Date.from(Instant.now())
        return user
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     */
    override fun validationUser(user: User): String? {
        return HibernateValidationUtil.validateEntity(user)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除用户(更新状态)
     *
     * @param id BigInteger
     * @return 用户ID  BigInteger
     */
    override fun removeBackId(id: BigInteger): BigInteger? {
        val user = User()
        user.id = id
        user.status = StatusEnum.DELETE.key
        user.gmtModified = Date.from(Instant.now())
        val backId: Int? = super.remove2StatusById(user)
        if (null != backId && 0 < backId) {
            return id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复用户(更新状态)
     *
     * @param id BigInteger
     * @return 用户ID  BigInteger
     */
    override fun recoverBackId(id: BigInteger): BigInteger? {
        val user = User()
        user.id = id
        user.status = StatusEnum.NORMAL.key
        user.gmtModified = Date.from(Instant.now())
        val backId: Int? = super.recover2StatusById(user)
        if (null != backId && 0 < backId) {
            return id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增用户
     *
     * @param user User
     * @return 用户ID  BigInteger
     */
    override fun saveBackId(user: User): BigInteger? {
        val id: Int? = super.save(user)
        if (null != id && 0 < id) {
            return user.id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新用户
     *
     * @param user User
     * @return 用户ID  BigInteger
     */
    override fun updateBackId(user: User): BigInteger? {
        val flag: Boolean = super.updateById(user)
        if (flag) {
            return user.id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据用户名查询 user
     *
     * @param username 用户名
     * @return User
     */
    override fun getByUsername(username: String): User? {
        val user = User()
        user.username = username
        return super.getByModel(user)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据昵称查询 user
     *
     * @param nickName 昵称
     * @return User
     */
    override fun getByNickName(nickName: String): User? {
        val user = User()
        user.nickName = nickName
        return super.getByModel(user)
    }

    /**
     * 根据电子邮箱查询 user
     *
     * @param email 电子邮箱
     * @return User
     */
    override fun getByEmail(email: String): User? {
        val user = User()
        user.email = email
        return super.getByModel(user)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据手机号码查询 user
     *
     * @param phone 手机号码
     * @return User
     */
    override fun getByPhone(phone: String): User? {
        val user = User()
        user.phone = phone
        return super.getByModel(user)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据用户名和密码查询 user
     *
     * @param username 用户名
     * @param password 密码
     * @return User
     */
    override fun getByUsernameAndPassword(username: String, password: String): User? {
        val user = User()
        user.username = username
        user.password = password
        return super.getByModel(user)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 登录
     *
     * @param username 用户名
     * @param password 密码
     * @return ResultData
     */
    override fun login(username: String, password: String): ResultData {
        val user: User? = this.getByUsernameAndPassword(username, password)
        return if (null != user) {
            if (StatusEnum.DISABLE.key == user.status) {
                ResultData.fail("message",
                        localeMessageSourceConfiguration.getMessage("message.user.login.disable.error"))
            }
            if (StatusEnum.DELETE.key == user.status) {
                ResultData.fail("message",
                        localeMessageSourceConfiguration.getMessage("message.user.login.delete.error"))
            }
            ResultData.ok("message", localeMessageSourceConfiguration.getMessage("message.user.login.succeed"))
        } else {
            ResultData.fail("message", localeMessageSourceConfiguration.getMessage("message.user.login.error"))
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 list 结果集
     *
     * @param columnArray 查询字段集合
     * @return List<T>
     */
    override fun list(id: BigInteger, columnArray: Array<String>?): List<User>? {
        val map: Map<String, Any> = mapOf(User.ID to id, User.USERNAME to "123456")
        return super.list(map, columnArray)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 数据加密
     *
     * @param user         用户
     * @return User
     */
    override fun aseEncryptData(user: User): User {
        // 密码
        user.password = digestManageConfiguration.getSha512(user.password!!)

        // 电子邮箱
        if (null != user.email && user.email!!.isNotBlank()) {
            user.email = aesManageConfiguration.encrypt(user.email!!)
        }

        // 手机号码
        if (null != user.phone && user.phone!!.isNotBlank()) {
            user.phone = aesManageConfiguration.encrypt(user.phone!!)
        }
        // 真实姓名
        if (null != user.realName && user.realName!!.isNotBlank()) {
            user.realName = aesManageConfiguration.encrypt(user.realName!!)
        }
        return user
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 数据解密
     *
     * @param user         用户
     * @return User
     */
    override fun aseDecryptData(user: User): User {

        // 电子邮箱
        if (null != user.email && user.email!!.isNotBlank()) {
            user.email = aesManageConfiguration.decrypt(user.email!!)
        }

        // 手机号码
        if (null != user.phone && user.phone!!.isNotBlank()) {
            user.phone = aesManageConfiguration.decrypt(user.phone!!)
        }
        // 真实姓名
        if (null != user.realName && user.realName!!.isNotBlank()) {
            user.realName = aesManageConfiguration.decrypt(user.realName!!)
        }
        return user
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End UserServiceImpl class

/* End of file UserServiceImpl.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/service/system/UserServiceImpl.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
